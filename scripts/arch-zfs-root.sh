#!/bin/bash

# check for input
if [ ! "${INST_HOSTNAME}" ]; then
	echo please export INST_HOSTNAME=new_hostname_for_installed_system
	exit 1
fi

# Prepare disk (259 is the code for nvme in this laptop)
DISK="/dev/$(lsblk --nodeps --include 259,8 --raw --noheadings -o NAME | head -1)"
echo "${DISK}"
sgdisk -o "${DISK}"                    # wipe the partition data
sgdisk -n1:1M:+256M -t1:EF00 "${DISK}" # for EFI
sgdisk -n2:0:0 -t2:8300 "${DISK}"      # everything else

# Setup luks encryption (to be replaces by native zfs encryption)
cryptsetup luksFormat -c aes-xts-plain64 -s 512 -h sha512 "${DISK}2"
cryptsetup luksOpen "${DISK}2" zroot_crypt

# Prepare zpool and unmountable datasets
zpool create -f -o ashift=12 -o cachefile=/tmp/zpool.cache -O atime=off -O canmount=off -O compression=lz4 -O normalization=formD -O mountpoint=/ -R /mnt zroot /dev/mapper/zroot_crypt
zfs create -o canmount=off -o mountpoint=none zroot/ROOT
zfs create -o canmount=off -o mountpoint=none zroot/ROOT/var
zfs create -o canmount=off -o mountpoint=none zroot/DATA

# Create mountable datasets
zfs create -o mountpoint=/ zroot/ROOT/default
zfs mount zroot/ROOT/default
zfs create -o mountpoint=/home -o setuid=off zroot/DATA/home
zfs create -o mountpoint=/root zroot/home/root
# Create the whole var setup
zfs create zroot/ROOT/var/log
zfs create zroot/ROOT/var/spool
zfs create -o com.sun:auto-snapshot=false zroot/ROOT/var/cache
zfs create -o com.sun:auto-snapshot=false -o exec=on zroot/ROOT/var/tmp
zfs create -o com.sun:auto-snapshot=false -o mountpoint=/var/lib/docker zroot/DOCKER
chmod 1777 /mnt/var/tmp

# why?
zfs set devices=off zroot

# Prepare boot disk
mkdosfs -F 32 -n EFI "${DISK}1"

# Setup the system
mkdir /mnt/boot
mount "${DISK}1" /mnt/boot/

# sign
pacman-key -r F75D9D76
pacman-key --lsign-key F75D9D76

# pacstrap archlinux
pacstrap -i /mnt base linux-lts zfs-linux-zfs intel-ucode iw wpa_supplicant sudo openssh networkmanager

# add archzfs repo
cat <<EOF >>/mnt/etc/pacman.conf
[archzfs]
Server = https://archzfs.com/$repo/$arch
EOF

# install bootloader
arch-chroot /mnt bootctl --path=/boot install

# configure bootloader
cat <<EOF >/mnt/boot/loader/loader.conf
default arch-lts
timeout 2
editor 0
EOF

cat <<EOF >/mnt/boot/loader/entries/arch-lts.conf
title   Arch Linux LTS
linux   /vmlinuz-linux-lts
initrd  /intel-ucode.img
initrd  /initramfs-linux-lts.img

# disk encryption
options cryptdevice=UUID=$(blkid -s UUID -o value "${DISK}2"):root_crypt

# zfs stuff
options zfs=rpool/ROOT/default 
options zfs.zfs_arc_max=536870912

# disable ipv6
options ipv6.disable=1
options ipv6.disable_ipv6=1

# everyone does this
options rw
EOF

# Help initrd startup zfs
cp /tmp/zpool.cache /mnt/etc/zfs/zpool.cache

# Set locale
sed -i 's/^#\(en_US.UTF-8 UTF-8\)/\1/' /mnt/etc/locale.gen
sed -i 's/^#\(en_GB.UTF-8 UTF-8\)/\1/' /mnt/etc/locale.gen
arch-chroot locale-gen
cat <<EOF >/mnt/etc/locale.conf
LANG=en_US.UTF-8
LC_TIME=en_GB.UTF-8
EOF

# Set hostname
echo "${INST_HOSTNAME}" >/mnt/etc/hostname

# Fix mkinitcpio hooks
sed -i 's/^\(HOOKS.*\) filesystems.*/\1 keyboard encrypt zfs filesystems)/' /mnt/etc/mkinitcpio.conf

# Rebuild image
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt mkinitcpio -p linux-lts

# Set root passwd
arch-chroot /mnt passwd

# Finally snapshot the whole installation, for fallback.
zfs snapshot zroot/ROOT/default@install

# Unmount && export (cleanup)
mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | xargs -i{} umount -lf {}
zpool export zroot
