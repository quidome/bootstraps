#!/bin/bash

# check for input
if [ ! "${IH}" ]; then
	echo please export IH=new_hostname_for_installed_system
	exit 1
fi

# Prepare disk (259 is the code for nvme in this laptop)
DISK="/dev/$(lsblk --nodeps --include 259,8 --raw --noheadings -o NAME | head -1)"
echo "${DISK}"
sgdisk -o "${DISK}"                    # wipe the partition data
sgdisk -n1:1M:+256M -t1:EF00 "${DISK}" # for EFI
sgdisk -n2:0:0 -t2:8e00 "${DISK}"      # everything else

# Setup luks encryption (to be replaces by native zfs encryption)
cryptsetup luksFormat -c aes-xts-plain64 -s 512 -h sha512 "${DISK}2"
cryptsetup luksOpen "${DISK}2" cryptlvm

# Prepare lvm
pvcreate /dev/mapper/cryptlvm
vgcreate MyVolGroup /dev/mapper/cryptlvm
lvcreate -L 8G MyVolGroup -n swap
lvcreate -L 64G MyVolGroup -n root
lvcreate -l 100%FREE MyVolGroup -n home

# format partitions
mkfs.ext4 /dev/MyVolGroup/root
mkfs.ext4 /dev/MyVolGroup/home
mkswap /dev/MyVolGroup/swap

# mounting
mount /dev/MyVolGroup/root /mnt
mkdir /mnt/home
mount /dev/MyVolGroup/home /mnt/home
swapon /dev/MyVolGroup/swap

# prepare boot disk
mkdosfs -F 32 -n EFI "${DISK}1"

# setup the system
mkdir /mnt/boot
mount "${DISK}1" /mnt/boot/

# pacstrap archlinux
pacstrap -i /mnt base linux linux-firmware lvm2 intel-ucode vim openssh dhcpcd wpa_supplicant

# write fstab
genfstab -U /mnt >> /mnt/etc/fstab

# set time
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
arch-chroot /mnt hwclock --systohc

# Set locale
sed -i 's/^#\(en_US.UTF-8 UTF-8\)/\1/' /mnt/etc/locale.gen
sed -i 's/^#\(en_IE.UTF-8 UTF-8\)/\1/' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
cat <<EOF >/mnt/etc/locale.conf
LANG=en_IE.UTF-8
EOF

# Set hostname
echo "${IH}" >/mnt/etc/hostname

# adds lvm required mkinitcpio hooks
sed -i 's/^\(HOOKS.*\) \(filesystems.*\)/\1 encrypt lvm2 \2/' /mnt/etc/mkinitcpio.conf
sed -i 's/^\(HOOKS=.*autodetect\) \(.*\)/\1 keyboard \2/' /mnt/etc/mkinitcpio.conf

# Rebuild image
arch-chroot /mnt mkinitcpio -p linux

echo Set root password for target system
arch-chroot /mnt passwd

# install bootloader
arch-chroot /mnt bootctl --path=/boot install

# configure bootloader
cat <<EOF >/mnt/boot/loader/loader.conf
default arch
timeout 4
editor 0
EOF

cat <<EOF >/mnt/boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img

# disk encryption
options cryptdevice=UUID=$(blkid -s UUID -o value "${DISK}2"):cryptlvm

# point to root device
options root=/dev/MyVolGroup/root

# disable ipv6
options ipv6.disable=1
options ipv6.disable_ipv6=1

# everyone does this
options rw
EOF

# enable services
arch-chroot /mnt systemctl enable sshd
